/*
* Input:
*	year  : 1800 - 2200
*	month : 1 - 12
*	day   : 1 - 31 
* Output:
*	char array: XXXX/XX/XX
*/

#ifndef _NEXT_DATE_H_
#define _NEXT_DATE_H_

const char* get_next_day(int y , int m, int d); // year, month, day

#endif
