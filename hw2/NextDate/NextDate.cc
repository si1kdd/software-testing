/*
* Implementation of Next Date Problem.
*/

#include <cstdio>
#include <cstdlib>
#include <cstring>

#include "NextDate.h"

char output[11];

int check_leap_year(int y)
{
	int leap = 0;
	if (y % 4 == 0) {
		if (y % 100 == 0) {
                        // clear code.
                        (y % 400 == 0) ? leap = 1 : leap = 0;
		}
		else
			leap = 1;
	}
	else
		leap = 0;

	return leap;
}

int get_day_of_month(int m)
{
	int mm = 0;
	int return_30[5] = {4, 6, 9, 11};
	int return_31[8] = {1, 3, 5, 7, 8, 10, 12};

	// check 30;
	for (int i = 0; i < 4; ++i)
		if (m == return_30[i])
			mm = 30;
	// check 31;
	for (int i = 0; i < 7; ++i)
		if (m == return_31[i])
			mm = 31;
        // don't need to 2/29 cases.
	return mm;
}

const char* get_next_day(int y, int m, int d)
{
	int leap = check_leap_year(y);
	if (y < 1812 || y > 2012 || m < 1 || m > 12 || d < 1 || d > 31)
		strcpy(output, "");
	else {

		if ( (d < 28) || ( d == 28 && m != 2 ) || ( d == 28 && m == 2 && leap == 1) ||
			(d == 29 && m != 2) || (d == 30 && get_day_of_month(m) == 31) )
		{
			sprintf(output, "%04d/%02d/%02d", y, m, d + 1);
		}
		else if ( ( d == 28 && m == 2 && leap == 0) ||
				( d == 29 && m == 2 && leap == 1) ||
					(d == 30 && get_day_of_month(m) == 30) ||
						(d == 31 && get_day_of_month(m) == 31 && m != 12) )
		{ // forget one case....
			sprintf(output, "%04d/%02d/%02d", y, m + 1, 1);
		}
		else if (m == 12 && d == 31)
		{
			sprintf(output, "%04d/%02d/%02d", y + 1, 1, 1);
		}
	}
	return output;
}

