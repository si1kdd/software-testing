#!/usr/bin/env bash

# suppose gcov exist.

echo "[*] Compiling..."
make clean; make

echo
echo "[*] Testing"
./nextdate_coveragetest

echo
echo "[*] Use gcov"
gcov -c ./NextDate.cc

echo
echo "[*] Reading the report of NextDate.cc"
cat NextDate.cc.gcov
