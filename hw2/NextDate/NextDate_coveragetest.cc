/*
 * NextDate Coverage test cases.
 */

#include <gtest/gtest.h>
#include "NextDate.h"

// Code Coverage C0:
//      statement coverage (SC), should cover every instruction.
// Should test all possible result.
TEST(CoverageTest, CodeCoverage_C0)
{
        // 2 results.
        EXPECT_STREQ(get_next_day(1912, -1, 15), "");                   // invaild date.
        EXPECT_STREQ(get_next_day(2000, 6, 14),  "2000/06/15");         // vaild date.
}

// Code Coverage C1:
//      Decision Coverage (DC), every branch, 3 input.
// Should have 5 branch to check.
//      1. Invalid or not?
//      2. Special Case. month + 1.
//      3. 12/31.        year + 1.
//      4. Normal        day + 1.
TEST(CoverageTest, CodeCoverage_C1)
{
        EXPECT_STREQ(get_next_day(1600, 2, 31),  "");                   // Invalid or not.
        EXPECT_STREQ(get_next_day(2000, 1, 30),  "2000/01/31");         // Normal day + 1. month have day 31.
        EXPECT_STREQ(get_next_day(2000, 4, 30),  "2000/05/01");         // Normal day + 1. month have day 30.
        EXPECT_STREQ(get_next_day(2000, 12, 31), "2001/01/01");         // Year + 1.
        EXPECT_STREQ(get_next_day(2001, 2, 28),  "2001/03/01");         // Month + 1.
        EXPECT_STREQ(get_next_day(2004, 2, 29),  "2004/03/01");         // Month + 1.
}

// Code Coverage C2:
//      every branch condition's result (true or false)?
TEST(CoverageTest, CodeCoverage_C2)
{
        EXPECT_STREQ(get_next_day(1600, 2, 31),  "");                   // Invalid or not.
        EXPECT_STREQ(get_next_day(2000, 0, 31),  "");                   // Invalid or not.
        EXPECT_STREQ(get_next_day(2000, 1, 0),   "");                   // Invalid or not.

        // Than C2 equals to C1.
        EXPECT_STREQ(get_next_day(2000, 1, 30),  "2000/01/31");         // Normal day + 1.
        EXPECT_STREQ(get_next_day(2000, 12, 31), "2001/01/01");         // Year + 1.
        EXPECT_STREQ(get_next_day(2001, 2, 28),  "2001/03/01");         // Month + 1.
}


// Code Coverage MCDC:
//      every point of entry and exit in the program has been invoked at least once.
TEST(CoverageTest, CodeCoverage_MCDC)
{
        // CC Min.
        EXPECT_STREQ(get_next_day(1811, 1, 1),   "");                   // Invalid or not.
        EXPECT_STREQ(get_next_day(2006, 0, 1),   "");                   // Invalid or not.
        EXPECT_STREQ(get_next_day(2000, 1, 0),   "");                   // Invalid or not.
        // CC Max.
        EXPECT_STREQ(get_next_day(2013, 1, 1),   "");                   // Invalid or not.
        EXPECT_STREQ(get_next_day(2006, 13, 1),  "");                   // Invalid or not.
        EXPECT_STREQ(get_next_day(2000, 1, 32),  "");                   // Invalid or not.

        // DC.
        EXPECT_STREQ(get_next_day(1600, 2, 31),  "");                   // Invalid or not.
        EXPECT_STREQ(get_next_day(2000, 1, 30),  "2000/01/31");         // Normal day + 1.
        EXPECT_STREQ(get_next_day(2000, 12, 31), "2001/01/01");         // Year + 1.
        EXPECT_STREQ(get_next_day(2001, 2, 28),  "2001/03/01");         // Month + 1.
        EXPECT_STREQ(get_next_day(2004, 2, 29),  "2004/03/01");         // Month + 1.
}

int main(int argc, char *argv[])
{
        ::testing::InitGoogleTest(&argc, argv);
        return RUN_ALL_TESTS();
}
