## TDD example : NextDate Problem Coverage Test cases ###

- - -

### Files ###
- NextDate.cc		        // target function will be tested.
- NextDate_coveragetest.cc   	// test cases with gtest.
- NextDate.h			        // function prototype.

### How to Run ###
- ./run.sh
