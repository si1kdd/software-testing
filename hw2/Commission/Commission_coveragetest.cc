/*
 * Commission Problem Coverage Test Cases.
 */

#include "Commission.h"
#include <gtest/gtest.h>

// Code Coverage C0:
//      statement coverage (SC), should cover every instruction.
//      2 instruction : invalid or not.
TEST(CoverageTest, CodeCoverage_C0)
{
        EXPECT_DOUBLE_EQ(get_commission(-1, -1, -1),  -1);
        EXPECT_DOUBLE_EQ(get_commission(10, 10, 10), 100);
}

// Code Coverage C1:
//      3 branch: invaild or not, min, max.
TEST(CoverageTest, CodeCoverage_C1)
{
        EXPECT_DOUBLE_EQ(get_commission(-1, -1, -1),    -1);
        EXPECT_DOUBLE_EQ(get_commission(10, 10, 9),     97.5);  // <= 1000.
        EXPECT_DOUBLE_EQ(get_commission(18, 18, 19),    225);   // > 1800
}

// Code Coverage C2:
//      every branch condition's result (true or false)?
//      C1 and check middle branch.
TEST(CoverageTest, CodeCoverage_C2)
{
        EXPECT_DOUBLE_EQ(get_commission(0, 0, 0),       -1);
        EXPECT_DOUBLE_EQ(get_commission(10, 10, 9),     97.5);  // <= 1000.
        EXPECT_DOUBLE_EQ(get_commission(18, 18, 19),    225);   // > 1800
        EXPECT_DOUBLE_EQ(get_commission(48, 48, 48),    820);   // (1000, 1800).
}


// Code Coverage MCDC:
//      every point of entry and exit in the program has been invoked at least once.
//      DC + CC
//      Here CC include:
//              1. Max/Min of a, b, c;
TEST(CoverageTest, CodeCoverage_MCDC)
{
        // CC
        EXPECT_DOUBLE_EQ(get_commission(0, 1, 1),       -1);
        EXPECT_DOUBLE_EQ(get_commission(1, 0, 1),       -1);
        EXPECT_DOUBLE_EQ(get_commission(1, 1, 0),       -1);
        EXPECT_DOUBLE_EQ(get_commission(71, 1, 1),      -1);
        EXPECT_DOUBLE_EQ(get_commission(1, 81, 1),      -1);
        EXPECT_DOUBLE_EQ(get_commission(1, 1, 91),      -1);

        // DC
        EXPECT_DOUBLE_EQ(get_commission(-1, -1, -1),    -1);
        EXPECT_DOUBLE_EQ(get_commission(10, 10, 9),     97.5);  // <= 1000.
        EXPECT_DOUBLE_EQ(get_commission(10, 10, 10),    100);   // = 1000.
        EXPECT_DOUBLE_EQ(get_commission(18, 18, 19),    225);   // > 1800
        EXPECT_DOUBLE_EQ(get_commission(18, 18, 18),    220);   // = 1800
}


int main(int argc, char *argv[])
{
        ::testing::InitGoogleTest(&argc, argv);
        return RUN_ALL_TESTS();
}
