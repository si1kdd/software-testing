### TDD example : Commission Problem Coverage Testing ##

### Files ###
- Commission.cc		        // target function will be tested.
- Commission_coveragetest_.cc	// test cases with gtest.
- Commission.h		        // define the function prototype and structures.

### How to Run ###
- ./run.sh
