#!/usr/bin/env bash

# suppose gcov exist.

echo "[*] Compiling..."
make clean; make

echo
echo "[*] Testing"
./commission_coveragetest

echo
echo "[*] Use gcov -c"
gcov -bc ./Commission.cc

echo
echo "[*] Reading the report of Commission.cc"
cat Commission.cc.gcov
