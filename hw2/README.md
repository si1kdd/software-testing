## Software testing hw2-stage1.
- - -

## Write the coverage test:
- Implement the C0, C1, C2 Coverage of those 3 problem.
- Implement MCDC (Modified Condition/Decision Coverage) coverage test.

## How to use gcov?.
- After running the execution file, it would generate a **.gcno file -> (gcov note file).

- Also generate a .gcda file (gcov data file).

- Than use gcov to record the source code.
   - gcov -b problem.cc -> generate an .c.gcov file (which is a record of code exection count).

## How to compile?
- just use the shell script and show the messages.

- - -

## Install googletest framework (Ubuntu). ##

- git clone https://github.com/google/googletest.git
- cd googletest/googletest/make
- make

## googletest marcos usage:
- TEST( TestCaseName, TestName ) {}
- EXPECT_EQ(expected, actual);
- EXPECT_DOUBLE_EQ(expected, actual);
