/*
 * Implement the C0, C1, C2 and MCDC coverage test of Triangle problem.
 */

#include "gtest/gtest.h"
#include "Triangle.h"

// Code Coverage C0:
//      statement coverage (SC), should cover every instruction.
// Should test all possible triangle result, let the program code run at least once.
TEST(CoverageTest, CodeCoverage_C0)
{
        EXPECT_EQ(None,         test_triangle(1, 1, 2));
        EXPECT_EQ(Isosceles,    test_triangle(5, 5, 6));
        EXPECT_EQ(Scalene,     test_triangle(5, 4, 3));
        EXPECT_EQ(Equilateral,  test_triangle(10, 10, 10));
}

// Code Coverage C1:
//      Decision Coverage (DC), every branch, 3 input.
//      1. check invaild or not?
//      2. 4 type of result.
//      total if/else branch :
//              1 + 4 = 5.
// Total test branch of triangle problem : 5 * 3 = 15.
TEST(CoverageTest, CodeCoverage_C1)
{
        // It should not be input the negative value, first branch.
        EXPECT_EQ(Invaild,      test_triangle(-1, -1, -2));
        // Second branch, is it a triangle ?.
        EXPECT_EQ(None,         test_triangle(1, 1, 2));
        // Than the branch of each triangle type checking.
        EXPECT_EQ(Isosceles,    test_triangle(5, 5, 6));
        EXPECT_EQ(Scalene,     test_triangle(5, 4, 3));
        EXPECT_EQ(Equilateral,  test_triangle(10, 10, 10));
}

// Code Coverage C2:
//      every branch condition's result (true or false)?
// In triagle problem, C1 equal to C2.
TEST(CoverageTest, CodeCoverage_C2)
{
        // It should not be input the negative value, first branch.
        // Including the Max and Min invalid input.
        EXPECT_EQ(Invaild,      test_triangle(0, -1, 201));
        // Second branch, is it a triangle.
        EXPECT_EQ(None,         test_triangle(1, 1, 2));
        // Than the branch of each triangle type checking.
        EXPECT_EQ(Isosceles,    test_triangle(5, 5, 6));
        EXPECT_EQ(Scalene,     test_triangle(5, 4, 3));
        EXPECT_EQ(Equilateral,  test_triangle(10, 10, 10));
}

// Code Coverage MCDC:
//      every point of entry and exit in the program has been invoked at least once.
//      DC + CC
//      Here CC include:
//              1. Max/Min, 2. edge equals, 3. edge plus + edge compare.
TEST(CoverageTest, CodeCoverage_MCDC)
{
        // 1. Max/Min and 0
        // F, F, F
        EXPECT_EQ(None,      test_triangle(0, 1, 201));
        EXPECT_EQ(None,      test_triangle(0, 201, 1));
        EXPECT_EQ(None,      test_triangle(201, 1, 0));
        // 1. Max/Min + edge equals.
        EXPECT_EQ(Invaild,      test_triangle(-1, -1, 201));
        EXPECT_EQ(Invaild,      test_triangle(-1, 201, -1));
        EXPECT_EQ(Invaild,      test_triangle(201, -1, -1));
        // 2. edge equlas.
        EXPECT_EQ(Equilateral,  test_triangle(10, 10, 10));
        // 3. edge compare.
        EXPECT_EQ(None,         test_triangle(1, 2, 3));
        EXPECT_EQ(None,         test_triangle(1, 3, 2));
        EXPECT_EQ(None,         test_triangle(3, 2, 1));
        // 3. edge plus + edge compare.
        EXPECT_EQ(Isosceles,    test_triangle(2, 2, 3));
        EXPECT_EQ(Isosceles,    test_triangle(2, 3, 2));
        EXPECT_EQ(Isosceles,    test_triangle(3, 2, 2));

        // DC test cases, remvoe the command case.
        // Than the branch of each triangle type checking.
        EXPECT_EQ(Scalene,     test_triangle(5, 4, 3));
        EXPECT_EQ(Equilateral,  test_triangle(10, 10, 10));
}

int main(int argc, char *argv[])
{
        ::testing::InitGoogleTest(&argc, argv);
        return RUN_ALL_TESTS();
}

