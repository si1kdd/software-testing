#!/usr/bin/env bash

# suppose gcov exist.

echo "[*] Compiling..."
make clean; make

echo
echo "[*] Testing"
./triangle_coveragetest

echo
echo "[*] Use gcov"
gcov -c ./Triangle.cc

echo
echo "[*] Reading the report of Triangle.cc"
cat Triangle.cc.gcov
