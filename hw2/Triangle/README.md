### TDD example : Triangle Problem Coverage Test cases ##

- - -

### Files ###
- Triangle.cc		        // target function will be tested.
- Triangle_coveragetest.cc	// test cases with gtest.
- Triangle.h			// define the function prototype and structures.

### How to Run ###

- ./run.sh                      // which is a shell script to use gcov and compile the program.
