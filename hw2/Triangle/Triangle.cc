/*
* Test cases first.
* Implement the triangle problem.
*/

#include "Triangle.h"
#define R1(x) ((x < MIN_SIDES) || (x > MAX_SIDES))
#define R2(a,b,c) ((a+b) <= c)
#define R3(a,b,c) (a < 0 || b < 0 || c < 0)

TriangleType test_triangle(int a, int b, int c)
{
        if ( R3(a, b, c) )
                return Invaild;
        else if ( R1(a) || R1(b) || R1(c) )
                return None;
        else if ( R2(a,b,c) || R2(b,c,a) || R2(a,c,b))
                return None;
        else if ( (a == b && a == c) || (b == a && b == c))
                return Equilateral;
        else if ( (a == b && b != c) || (a == c && a != b) || (b == c && a != b) )
                return Isosceles;
        else
                return Scalene;
}
