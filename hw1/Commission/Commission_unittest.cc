/*
 * TDD Driven, Write Test Cases first.
 */

#include "gtest/gtest.h"
#include "Commission.h"

TEST(BoundaryValue, Normal)
{
        // min
        EXPECT_DOUBLE_EQ(get_commission(1, 1, 1), 10);
        // min + 1
        EXPECT_DOUBLE_EQ(get_commission(1, 1, 2), 12.5);
        EXPECT_DOUBLE_EQ(get_commission(1, 2, 1), 13);
        EXPECT_DOUBLE_EQ(get_commission(2, 1, 1), 14.5);
        // middle
        EXPECT_DOUBLE_EQ(get_commission(5, 5, 5), 50);
        // border point - 1.
        EXPECT_DOUBLE_EQ(get_commission(10, 10 ,9), 97.5);
        EXPECT_DOUBLE_EQ(get_commission(10, 9, 10), 97);
        EXPECT_DOUBLE_EQ(get_commission(9, 10, 10), 95.5);
        // border point.
        EXPECT_DOUBLE_EQ(get_commission(10, 10, 10), 100);
        // border point + 1.
        EXPECT_DOUBLE_EQ(get_commission(10, 10, 11), 103.75);
        EXPECT_DOUBLE_EQ(get_commission(10, 11, 10), 104.5);
        EXPECT_DOUBLE_EQ(get_commission(11, 10, 10), 106.75);
        // middle
        EXPECT_DOUBLE_EQ(get_commission(14, 14, 14), 160);
        // border point - 1.
        EXPECT_DOUBLE_EQ(get_commission(18, 18, 17), 216.25);
        EXPECT_DOUBLE_EQ(get_commission(18, 17, 18), 215.5);
        EXPECT_DOUBLE_EQ(get_commission(17, 18, 18), 213.25);
        // border point.
        EXPECT_DOUBLE_EQ(get_commission(18, 18, 18), 220);
        // border point + 1.
        EXPECT_DOUBLE_EQ(get_commission(18, 18, 19), 225);
        EXPECT_DOUBLE_EQ(get_commission(18, 19, 18), 226);
        EXPECT_DOUBLE_EQ(get_commission(19, 18, 18), 229);
        // middle
        EXPECT_DOUBLE_EQ(get_commission(48, 48, 48), 820);
        // max - 1.
        EXPECT_DOUBLE_EQ(get_commission(70, 80, 89), 1415);
        EXPECT_DOUBLE_EQ(get_commission(70, 79, 90), 1414);
        EXPECT_DOUBLE_EQ(get_commission(69, 80, 90), 1411);
        // max.
        EXPECT_DOUBLE_EQ(get_commission(70, 80, 90), 1420);
}

TEST(Equivalence, WeakRobust)
{
        // WR1 - WR8
        EXPECT_DOUBLE_EQ(get_commission(10, 10, 10), 100);
        EXPECT_DOUBLE_EQ(get_commission(-1, 40, 45), -1);
        EXPECT_DOUBLE_EQ(get_commission(-2, 40, 45), -1);
        EXPECT_DOUBLE_EQ(get_commission(71, 40, 45), -1);
        EXPECT_DOUBLE_EQ(get_commission(35, -1, 45), -1);
        EXPECT_DOUBLE_EQ(get_commission(35, 81, 45), -1);
        EXPECT_DOUBLE_EQ(get_commission(35, 40, -1), -1);
        EXPECT_DOUBLE_EQ(get_commission(35, 40, 91), -1);
}

TEST(Equivalence, StrongRobust)
{
        // SR1 - SR7
        EXPECT_DOUBLE_EQ(get_commission(-2, 40, 45), -1);
        EXPECT_DOUBLE_EQ(get_commission(35, -1, 45), -1);
        EXPECT_DOUBLE_EQ(get_commission(-2, 40, -2), -1);
        EXPECT_DOUBLE_EQ(get_commission(-2, -1, 45), -1);
        EXPECT_DOUBLE_EQ(get_commission(-2, 40, -1), -1);
        EXPECT_DOUBLE_EQ(get_commission(35, -1, -1), -1);
        EXPECT_DOUBLE_EQ(get_commission(-2, -1, -1), -1);
}

TEST(Equivalence, OutputRange)
{
        // OR1 - OR3
        EXPECT_DOUBLE_EQ(get_commission(5, 5, 5), 50);
        EXPECT_DOUBLE_EQ(get_commission(15, 15, 15), 175);
        EXPECT_DOUBLE_EQ(get_commission(25, 25, 25), 360);
}

// The commission problem is not well served by a secision table analysis, very little decisional logic is used in the problem.

int main(int argc, char *argv[])
{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}	
