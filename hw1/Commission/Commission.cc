/*
* Implement the Commission Problem
*/

#include "Commission.h"

double get_commission(int l, int s, int b)
{
	double commission = 0.0;
	if (l < 1 || l > 70 || s < 1 || s > 80 || b < 1 || b > 90)
		return -1.0;
	else {
		double cost = l * 45.0 + s * 30.0 + b * 25.0;
		if (cost <= 1000.0)
			commission = 0.10 * cost;
		else if (cost > 1800.0)
			commission = 220.0 + 0.20 * (cost - 1800.0);
		else
			commission = 100.0 + 0.15 * (cost - 1000.0);

		return commission;
	}
}

