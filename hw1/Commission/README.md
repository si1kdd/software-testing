### TDD example : Commission Problem ##

### Files ###
	- 1. Commission.cc		// target function will be tested.
	- 2. Commission_unittest.cc	// test cases with gtest.
	- 3. Commission.h		// define the function prototype and structures.

### How to Run ###
	- make; ./commission_unittest
