/*
* define the triangle conditions.
*/

#ifndef _TRIANGLE_H_
#define _TRIANGEL_H__

#define MAX_SIDES 200
#define MIN_SIDES 1

enum TriangleType { None = 0, Equilateral = 1, Isosceles = 2, Scalene = 3 };

/* prototype of test function , input the sides of a triangle */
TriangleType test_triangle(int a, int b, int c);

#endif
