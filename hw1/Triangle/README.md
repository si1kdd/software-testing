### TDD example : Triangle Problem ##

### Files ###
	- 1. Triangle.cc		// target function will be tested.
	- 2. Triangle_unittest.cc	// test cases with gtest.
	- 3. Triangle.h			// define the function prototype and structures.

### How to Run ###
	- make; ./triangle_unittest
