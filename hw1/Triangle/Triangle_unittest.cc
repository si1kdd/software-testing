/*
* use gtest.
* TDD driven, write Test Cases first.
*/

#include "gtest/gtest.h"
#include "Triangle.h"

/* EXPECT_EQ(expected_value, actual_value); */

TEST(BoundaryValueTest, NormalBoundary)
{
	/* have to finish this case */
	ASSERT_EQ(Equilateral, test_triangle(100, 100, 100));

	/* just one minimum sides bound */
	EXPECT_EQ(Isosceles, test_triangle(MIN_SIDES, 100, 100));
	EXPECT_EQ(Isosceles, test_triangle(100, MIN_SIDES, 100));
	EXPECT_EQ(Isosceles, test_triangle(100, 100, MIN_SIDES));
	/* just one max sides bound */
	EXPECT_EQ(None, test_triangle(MAX_SIDES, 100, 100));
	EXPECT_EQ(None, test_triangle(100, MAX_SIDES, 100));
	EXPECT_EQ(None, test_triangle(100, 100, MAX_SIDES));

	/* minimum + 1 sides bound */
	EXPECT_EQ(Isosceles, test_triangle(MIN_SIDES + 1, 100, 100));
	EXPECT_EQ(Isosceles, test_triangle(100, MIN_SIDES + 1, 100));
	EXPECT_EQ(Isosceles, test_triangle(100, 100, MIN_SIDES + 1));
	/* minimum - 1 sides bound */
	EXPECT_EQ(None, test_triangle(MIN_SIDES - 1, 100, 100));
	EXPECT_EQ(None, test_triangle(100, MIN_SIDES - 1, 100));
	EXPECT_EQ(None, test_triangle(100, 100, MIN_SIDES - 1));

	/* maximum - 1 sides bound */
	EXPECT_EQ(Isosceles, test_triangle(MAX_SIDES - 1, 100, 100));
	EXPECT_EQ(Isosceles, test_triangle(100, MAX_SIDES - 1, 100));
	EXPECT_EQ(Isosceles, test_triangle(100, 100, MAX_SIDES - 1));
	/* maximum + 1 sides bound */
	EXPECT_EQ(None, test_triangle(MAX_SIDES + 1, 100, 100));
	EXPECT_EQ(None, test_triangle(100, MAX_SIDES + 1, 100));
	EXPECT_EQ(None, test_triangle(100, 100, MAX_SIDES + 1));

	/* Scalene cases */
	EXPECT_EQ(Scalene, test_triangle(3, 4, 5));
}

TEST(EquivalenceClass, WeakRobust)
{
	/* Not a triangle a <= 0, b <= 0, c <= 0*/
	for (int i = -1; i <= 0; ++i)
	{
		EXPECT_EQ(None, test_triangle(i, 2 ,2));
		EXPECT_EQ(None, test_triangle(1, i ,2));
		EXPECT_EQ(None, test_triangle(1, 2 ,i));
	}
	/* Not a triangle a + b <= c, b + c < = a, c + a <= b*/
	for (int i = 1; i <= 2; ++i)
	{
		EXPECT_EQ(None, test_triangle(i, 1, 3));
		EXPECT_EQ(None, test_triangle(1, i, 3));
		EXPECT_EQ(None, test_triangle(1, 3, i));
	}
	/* Not a Isosceles a != b && b != c && c != a*/
	ASSERT_EQ(Scalene, test_triangle(30, 40, 50));
}

TEST(EquivalenceClass, WeakNormal)
{
	/* is a Scalene Triangle a > 0, b > 0, c > 0, a + b > c, b + c > a, a + c > b */
	ASSERT_EQ(Scalene, test_triangle(3, 4 ,5));
	/* is a Equilateral Triangle a = b = c */
	ASSERT_EQ(Equilateral, test_triangle(10, 10 ,10));
	/* is a Isosceles Triangle a = b = c */
	ASSERT_EQ(Isosceles, test_triangle(11, 11 , 20));
	ASSERT_EQ(Isosceles, test_triangle(11, 20 , 11));
	ASSERT_EQ(Isosceles, test_triangle(20, 11 , 11));
}

TEST(DecisionTable, Normal)
{
	EXPECT_EQ(None, test_triangle(4, 1, 2));
	EXPECT_EQ(None, test_triangle(1, 4, 2));
	EXPECT_EQ(None, test_triangle(1, 2, 4));
	EXPECT_EQ(Equilateral, test_triangle(5, 5, 5));
	/* impossible 5 and 6*/
	EXPECT_EQ(Isosceles, test_triangle(2, 2, 3));
	/* impossible 7 */
	EXPECT_EQ(Isosceles, test_triangle(2, 3, 3));
	EXPECT_EQ(Isosceles, test_triangle(3, 2, 2));
	EXPECT_EQ(Scalene, test_triangle(3, 4, 5));
}

int main(int argc, char *argv[])
{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
