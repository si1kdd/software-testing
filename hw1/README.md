## Book reading progress ##
	- Chapter 1 ~ Chapter 11...
	- Books Note of Chapter 1 - Chapter 8:
	    - https://hackmd.io/s/S1nWhKETl#

## Install googletest framework (Ubuntu). ##

	- git clone https://github.com/google/googletest.git
	- cd googletest/googletest/make
	- make

## googletest marcos usage:
	- TEST( TestCaseName, TestName ) {}
	- EXPECT_EQ(expected, actual);
	- EXPECT_DOUBLE_EQ(expected, actual);
