## TDD example : NextDate ###
=======

### Files ###
	- 1. NextDate.cc		// target function will be tested.
	- 2. NextDate_unittest.cc   	// test cases with gtest.
	- 3. NextDate.h			// function prototype.

### How to Run ###
	- make ; ./nextdate_unittest
