/*
* TDD Driven, Write Test Cases first.
*/

#include <gtest/gtest.h>
#include "NextDate.h"

/* check string */
/* EXPECT_STREQ(expected_str, actual_str) */

/* BVT */
TEST(BoundaryValue, Normal)
{
	// basic Test Cases.
	// year.
	// min year
	EXPECT_STREQ(get_next_day(1812, 6, 15), "1812/06/16");
	// min+1 year.
	EXPECT_STREQ(get_next_day(1813, 6, 15), "1813/06/16");
	// middle year.
	EXPECT_STREQ(get_next_day(1912, 6, 15), "1912/06/16");
	// max-1 year.
	EXPECT_STREQ(get_next_day(2011, 6, 15), "2011/06/16");
	// max year.
	EXPECT_STREQ(get_next_day(2012, 6, 15), "2012/06/16");
	// day.
	// min day.
	EXPECT_STREQ(get_next_day(1912, 6, 1), "1912/06/02");
	// min+1 day.
	EXPECT_STREQ(get_next_day(1912, 6, 2), "1912/06/03");
	// max-1 day.
	EXPECT_STREQ(get_next_day(1912, 6, 30), "1912/07/01");
	// max day.
	EXPECT_STREQ(get_next_day(1912, 6, 31), "");
	// month
	// min month
	EXPECT_STREQ(get_next_day(1912, 1, 15), "1912/01/16");
	// min + 1 month
	EXPECT_STREQ(get_next_day(1912, 2, 15), "1912/02/16");
	// max - 1 month
	EXPECT_STREQ(get_next_day(1912, 11, 15), "1912/11/16");
	// max month
	EXPECT_STREQ(get_next_day(1912, 12, 15), "1912/12/16");
}

/* ECT */
TEST(Equivalence, Normal)
{
	// weak Normal and strong Normal
	EXPECT_STREQ(get_next_day(1912, 6, 15), "1912/06/16");
}

TEST(Equivalence, WeakRobust)
{
	EXPECT_STREQ(get_next_day(1912, -1, 15), "");
	EXPECT_STREQ(get_next_day(1912, 13, 15), "");
	EXPECT_STREQ(get_next_day(1912, 6, -1), "");
	EXPECT_STREQ(get_next_day(1912, 6, 32), "");
	EXPECT_STREQ(get_next_day(1811, 6, 15), "");
	EXPECT_STREQ(get_next_day(2013, 6, 15), "");
}

TEST(Equivalence, StrongRobust)
{
	EXPECT_STREQ(get_next_day(1912, -1, 15), "");
	EXPECT_STREQ(get_next_day(1912, 6, -1), "");
	EXPECT_STREQ(get_next_day(1811, 6, 15), "");
	EXPECT_STREQ(get_next_day(1912, 6, -1), "");
	EXPECT_STREQ(get_next_day(1811, -1, 15), "");
	EXPECT_STREQ(get_next_day(1811, -1, -1), "");
}

TEST(Equivalence, WeakNormal)
{
	// WN1 - WN4
	EXPECT_STREQ(get_next_day(2000, 6, 14), "2000/06/15");
	EXPECT_STREQ(get_next_day(1996, 7, 29), "1996/07/30");
	EXPECT_STREQ(get_next_day(2002, 2, 30), "");
	EXPECT_STREQ(get_next_day(2000, 6, 31), "");
}

TEST(Equivalence, StrongNormal)
{
	//SN1 - SN26
	EXPECT_STREQ(get_next_day(2000, 6, 14), "2000/06/15");
	EXPECT_STREQ(get_next_day(1996, 6, 14), "1996/06/15");
	EXPECT_STREQ(get_next_day(2002, 6, 14), "2002/06/15");
	EXPECT_STREQ(get_next_day(2000, 6, 29), "2000/06/30");
	EXPECT_STREQ(get_next_day(1996, 6, 29), "1996/06/30");
	EXPECT_STREQ(get_next_day(2002, 6, 29), "2002/06/30");
	EXPECT_STREQ(get_next_day(2000, 6, 30), "2000/07/01");
	EXPECT_STREQ(get_next_day(1996, 6, 30), "1996/07/01");
	EXPECT_STREQ(get_next_day(2002, 6, 31), "");
	EXPECT_STREQ(get_next_day(2000, 6, 31), "");
	EXPECT_STREQ(get_next_day(1996, 6, 31), "");
	EXPECT_STREQ(get_next_day(2002, 6, 31), "");
	EXPECT_STREQ(get_next_day(2000, 7, 14), "2000/07/15");
	EXPECT_STREQ(get_next_day(1996, 7, 14), "1996/07/15");
	EXPECT_STREQ(get_next_day(2002, 7, 14), "2002/07/15");
	EXPECT_STREQ(get_next_day(2000, 7, 29), "2000/07/30");
	EXPECT_STREQ(get_next_day(1996, 7, 29), "1996/07/30");
	EXPECT_STREQ(get_next_day(2002, 7, 29), "2002/07/30");
	EXPECT_STREQ(get_next_day(2000, 7, 30), "2000/07/31");
	EXPECT_STREQ(get_next_day(1996, 7, 30), "1996/07/31");
	EXPECT_STREQ(get_next_day(2002, 7, 30), "2002/07/31");
	EXPECT_STREQ(get_next_day(2000, 7, 31), "2000/08/01");
	EXPECT_STREQ(get_next_day(1996, 7, 31), "1996/08/01");
	EXPECT_STREQ(get_next_day(2002, 7, 31), "2002/08/01");
	EXPECT_STREQ(get_next_day(2000, 2, 14), "2000/02/15");
	EXPECT_STREQ(get_next_day(2000, 2, 14), "2000/02/15");
}

/* DTT */
TEST(DecisionTable, Normal)
{
	EXPECT_STREQ(get_next_day(2001, 4, 15), "2001/04/16");
	EXPECT_STREQ(get_next_day(2001, 4, 30), "2001/05/01");
	EXPECT_STREQ(get_next_day(2001, 4, 31), "");
	EXPECT_STREQ(get_next_day(2001, 1, 15), "2001/01/16");
	EXPECT_STREQ(get_next_day(2001, 1, 31), "2001/02/01");
	EXPECT_STREQ(get_next_day(2001, 12, 15), "2001/12/16");
	EXPECT_STREQ(get_next_day(2001, 12, 31), "2002/01/01");
	EXPECT_STREQ(get_next_day(2001, 2, 15), "2001/02/16");
	EXPECT_STREQ(get_next_day(2004, 2, 28), "2004/02/29");
	EXPECT_STREQ(get_next_day(2001, 2, 28), "2001/03/01");
	EXPECT_STREQ(get_next_day(2004, 2, 29), "2004/03/01");
	EXPECT_STREQ(get_next_day(2001, 2, 29), "");
	EXPECT_STREQ(get_next_day(2001, 2, 30), "");
}

int main(int argc, char *argv[])
{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
