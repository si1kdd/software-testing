#!/usr/bin/env bash

# suppose klee exist.

echo "[*] Compiling..."
make clean; make

echo
echo "[*] Testing"
klee --optimize --libc=uclibc --posix-runtime --allow-external-sym-calls klee_nextdate.bc | tee result.txt
