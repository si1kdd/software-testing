## NextDate Problem With Symbolic and Fuzz testing ###

- - -

### Files ###
- NextDate_AFL.c; NextDate_KLEE.c 
	- target function will be tested.
- NextDate.h			       
	- function prototype.

### How to Run ###
- ./run_klee.sh
- ./run_AFL.sh

- make clean

- - -

### Files ### 
- LocalReult/ is my klee result files. 
