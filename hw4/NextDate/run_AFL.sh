#!/usr/bin/env bash

# suppose AFL exist.
AFL_FUZZ=../AFL/afl-2.41b/afl-fuzz
AFL_CC=../AFL/afl-2.41b/afl-gcc

echo "[*] afl-gcc Compiling..."
$AFL_CC NextDate_AFL.c -o afl_nextdate -Wall -O0

echo "[*] afl-fuzz fuzzer testing"
$AFL_FUZZ -i testcase/ -o output ./afl_nextdate
