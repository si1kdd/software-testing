### Triangle Problem with Symbolic and Fuzz Test ##

- - -

### Files ###
- Triangle.c
	- target function will be tested.
- Triangle.
	- define the function prototype and structures.

- - -

### How to Run This Program ###

- ./run_klee.sh                      
	- which is a shell script to use KLEE.
- ./run_AFL.sh
	- which is a shell script to use AFL.

- make clean
	- It would clean all klee and afl files.

- - -

### Files ###
- testcase/ folder is for AFL test cases files.
- LocalResults/ is my klee and AFL result files, testing on my local machine.
	- output/ folder is the AFL output files.
	- the other are KLEE files.
