#!/usr/bin/env bash

dir=`pwd`
echo "[!] Suppose You are using Ubuntu"
echo

echo "[*] Install docker.io"
apt-get install docker.io

echo

echo "[*] Pull a KLEE image"
docker pull klee/klee

echo

echo "[*] Running the KLEE image and mount the folder"
docker run --rm -ti -v $dir/:/home/test --ulimit='stack=-1:-1' klee/klee
