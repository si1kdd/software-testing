/*
 * Input: locks, stocks, barrels.
 * Output: money (commission) sales should take.
 * -1 = error.
 */

#ifndef _COMMISSION_H_
#define _COMMISSION_H_

double get_commission(int l, int s, int b);

#endif
