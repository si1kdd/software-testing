## Commission Problem With Symbolic and Fuzz testing ###

- - -

### Files ###
- Commission_AFL.c; Commission_KLEE.c  
	- target function will be tested.
- Commission.h			       
	- function prototype.

### How to Run ###
- ./run_klee.sh
- ./run_AFL.sh
- make clean

- - -

### Files ### 
- LocalReult/ is my local test result files. 
