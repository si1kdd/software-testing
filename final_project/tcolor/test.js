'use strict';
var assert = require('assert');
var tcolor = require('./');

  // prints the entire color palette
  (function () {
    console.log('\x1b[38;5;255m\x1b[4;37m Default palette:\x1b[0m');
    tcolor.bg.standard.forEach(function (c, i) { process.stdout.write(c + 'defaults ' + i + '  '); });

    // reset
    console.log(tcolor.reset + '\n');
    console.log('\x1b[38;5;255m\x1b[4;37m Bright palette:\x1b[0m');

    // bright color testing.
    tcolor.bg.bright.forEach(function (c, i) { process.stdout.write(c + 'bright ' + i + '    '); });
    console.log(tcolor.reset + '\n');

    console.log('\x1b[38;5;255m\x1b[4;37m RGB ranges 0 to 6:\x1b[0m');
    for (var r = 0; r < 6; r++) {
      for (var g = 0; g < 6; g++) {
        for (var b = 0; b < 6; b++) {
          process.stdout.write(tcolor.bg.getRgb(r, g, b) + 'r:' + r + ' g:' + g + ' b:' + b + ' ');
        }
        process.stdout.write('\n');
      }
      if (r !== 5) process.stdout.write('\n');
    }
    console.log(tcolor.reset);

    console.log('\x1b[38;5;255m\x1b[4;37mGrayscales:\x1b[0m');
    tcolor.bg.grayscale.forEach(function (c, i) {
      process.stdout.write(c + 'gray ' + (i < 10 ? '0' : '') + i + '     ' + ((i+1) % 6 ? '' : '\n'));
    });
    console.log(tcolor.reset);
  }());

describe('user wrong input', function () {
  it('wrong input', function() {
     assert.equal(tcolor.bg.codes[-1], undefined);
     assert.equal(tcolor.fg.codes[-1], undefined);
     assert.equal(tcolor.bg[-1], undefined);
     console.log('User Input invaild !!');
  });
});

// BVT.
describe('BVT robust testing', function() {
  // fg.
  it('16m max and min fg', function() {
   assert.equal(tcolor.fg.codes[30], '\u001b[38;5;30m');
   assert.equal(tcolor.fg.codes[37], '\u001b[38;5;37m');
  });
  it('256 max and min fg', function() {
   assert.equal(tcolor.fg.codes[0], '\u001b[38;5;0m');
   assert.equal(tcolor.fg.codes[255], '\u001b[38;5;255m');
  });
  it('high contrast colors max and min fg', function() {
   assert.equal(tcolor.fg.codes[90], '\u001b[38;5;90m');
   assert.equal(tcolor.fg.codes[97], '\u001b[38;5;97m');
  });
  // bg.
  it('16m max and min bg', function() {
   assert.equal(tcolor.bg.codes[30], '\u001b[48;5;30m');
   assert.equal(tcolor.bg.codes[37], '\u001b[48;5;37m');
  });
  it('256 max and min bg', function() {
   assert.equal(tcolor.bg.codes[0], '\u001b[48;5;0m');
   assert.equal(tcolor.bg.codes[255], '\u001b[48;5;255m');
  });
  it('high contrast colors max and min bg', function() {
   assert.equal(tcolor.bg.codes[90], '\u001b[48;5;90m');
   assert.equal(tcolor.bg.codes[97], '\u001b[48;5;97m');
  });
  // underline.
  it('16m max and min underline', function() {
   assert.equal(tcolor.fg.under[30], '\u001b[38;4;30m');
   assert.equal(tcolor.fg.under[37], '\u001b[38;4;37m');
   assert.equal(tcolor.bg.under[30], '\u001b[38;4;30m');
   assert.equal(tcolor.bg.under[37], '\u001b[38;4;37m');
  });
  it('256 max and min underline', function() {
   assert.equal(tcolor.fg.under[0], '\u001b[38;4;0m');
   assert.equal(tcolor.fg.under[255], '\u001b[38;4;255m');
   assert.equal(tcolor.bg.under[0], '\u001b[38;4;0m');
   assert.equal(tcolor.bg.under[255], '\u001b[38;4;255m');
  });
  it('high contrast colors max and min underline' , function() {
   assert.equal(tcolor.fg.under[90], '\u001b[38;4;90m');
   assert.equal(tcolor.fg.under[97], '\u001b[38;4;97m');
   assert.equal(tcolor.bg.under[90], '\u001b[38;4;90m');
   assert.equal(tcolor.bg.under[97], '\u001b[38;4;97m');
  });
  // bright color input.
  it('bright should be in 8 - 16 of 256 color', function() {
   assert.equal(tcolor.fg.bright[0], '\u001b[38;5;8m');
   assert.equal(tcolor.fg.bright[tcolor.fg.bright.length - 1], '\u001b[38;5;15m');
  });
  // Min - 1.
  it('8, 16, 256 min - 1', function() {
   assert.equal(tcolor.fg.codes[29], '\u001b[38;5;29m');
   assert.equal(tcolor.fg.codes[89], '\u001b[38;5;89m');
   assert.equal(tcolor.fg.codes[-1], undefined);
  });
  // Max - 1.
  it('8, 16, 256 max - 1', function() {
   assert.equal(tcolor.fg.codes[36], '\u001b[38;5;36m');
   assert.equal(tcolor.fg.codes[96], '\u001b[38;5;96m');
   assert.equal(tcolor.fg.codes[254],'\u001b[38;5;254m');
  });
  it('8, 16, 256 min + 1', function() {
   assert.equal(tcolor.fg.codes[31], '\u001b[38;5;31m');
   assert.equal(tcolor.fg.codes[91], '\u001b[38;5;91m');
   assert.equal(tcolor.fg.codes[1],  '\u001b[38;5;1m');
  });
  it('8, 16, 256 max + 1', function() {
   assert.equal(tcolor.fg.codes[38], '\u001b[38;5;38m');
   assert.equal(tcolor.fg.codes[98], '\u001b[38;5;98m');
   assert.equal(tcolor.fg.codes[256],  undefined);
  });
});

// ECT.
describe('ECT testing', function() {	
  it('divied 256 into 4 class', function() {
	var one = 0 + 64;
	var two = one + 64;
	var three = two + 64;
	var four = three + 64;
	var i = 0;
		for (i = 0; i < one; i++) {
   			assert.equal(tcolor.fg.codes[i], '\u001b[38;5;' + i + 'm');
   			assert.equal(tcolor.bg.codes[i], '\u001b[48;5;' + i + 'm');
		}
		for (i = one; i < two; i++) {
   			assert.equal(tcolor.fg.codes[i], '\u001b[38;5;' + i + 'm');
   			assert.equal(tcolor.bg.codes[i], '\u001b[48;5;' + i + 'm');
		}
		for (i = two; i < three; i++) {
   			assert.equal(tcolor.fg.codes[i], '\u001b[38;5;' + i + 'm');
   			assert.equal(tcolor.bg.codes[i], '\u001b[48;5;' + i + 'm');
		}
		for (i = three; i < four; i++) {
   			assert.equal(tcolor.fg.codes[i], '\u001b[38;5;' + i + 'm');
   			assert.equal(tcolor.bg.codes[i], '\u001b[48;5;' + i + 'm');
		}
  });
  it('divied 256 into 2 class', function() {
	var one = 0 + 128;
	var two = one + 128;
		var i = 0;
		for (i = 0; i < one; i++) {
   			assert.equal(tcolor.fg.codes[i], '\u001b[38;5;' + i + 'm');
   			assert.equal(tcolor.bg.codes[i], '\u001b[48;5;' + i + 'm');
		}
  });
});



describe('256-colors, fg, grayscale, rgb function testing', function () {
  it('foreground colours testing', function () {
    assert.equal(tcolor.fg.standard[5], '\u001b[38;5;5m');
    assert.equal(tcolor.fg.grayscale[3], '\u001b[38;5;235m');
    assert.equal(tcolor.fg.rgb[44], '\u001b[38;5;60m');
  });

  it('background colours testing', function () {
    assert.equal(tcolor.bg.standard[5], '\u001b[48;5;5m');
    assert.equal(tcolor.bg.grayscale[3], '\u001b[48;5;235m');
    assert.equal(tcolor.bg.rgb[44], '\u001b[48;5;60m');
  });

  it('retrieve the corresponding colors on getRgb', function () {
    assert.equal(tcolor.fg.getRgb(3, 3, 3), '\u001b[38;5;145m');
    assert.equal(tcolor.bg.getRgb(3, 3, 3), '\u001b[48;5;145m');
  });
});
