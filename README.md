# Software Testing Homeworks of NCTU #

## hw1 ##

* Pratice TDD
* Pratice google testing framework
* Implement the Triangle, NextDate, Commission problems by TDD.

- - -

## hw2 ##
* Pratice TDD
* Implement the Coverage analysis of hw1 problems.
* Get familiar with gcov.

- TODO:
    - using python to implement the coverage test cases.

- - -

## hw4 ##
* Pratice Symbolic testing and Fuzz testing.

* Using KLEE, afl on hw1 problem.

* Study fuzz testing tools: AFL, AFLfast, BFF, FOE. 
* Study Symbolic Integrated Fuzz testing tools: driller (angr + afl), s2e + afl
